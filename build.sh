#! /bin/bash

if [ -f "common-config.sh" ]; then
    source common-config.sh
else
    echo "common-config.sh does not exist"
    exit 1
fi

if [ -f "build-config.sh" ]; then
    source build-config.sh
else
    echo "build-config.sh does not exist"
    exit 1
fi

sudo -E bash -c "apt-get -y install docker python-pip"
sudo -E bash -c "pip install docker-compose"

mkdir -p ./${SRC_REL_PATH}
mkdir -p ./${OUT_REL_PATH}

if [ ! -f "${SDK_REL_PATH}.tar.gz" ]; then
    if [ -d "${SDK_REL_PATH}" ]; then
        echo "Creating ${SDK_REL_PATH}.tar.gz..."
        tar -czf ${SDK_REL_PATH}.tar.gz ${SDK_REL_PATH}
        rm -rf ${SDK_REL_PATH}
    else
        echo "Neither ${SDK_REL_PATH}.tar.gz nor ${SDK_REL_PATH} exists"
        exit 1
    fi
fi

if [ ! -d "${SRC_REL_PATH}/rootfs" ]; then
    if [ -f "${TGTFS_NAME}.tar.gz" ]; then
        echo "Uncompressing ${TGTFS_NAME}.tar.gz to ${SRC_REL_PATH}/rootfs..."
        tar -xzf ${TGTFS_NAME}.tar.gz ${TGTFS_NAME}
        mv ${TGTFS_NAME} ${SRC_REL_PATH}/rootfs
    else
        echo "Neither ${TGTFS_NAME}.tar.gz nor ${SRC_REL_PATH}/rootfs exists"
        exit 1
    fi
fi

sudo -E bash -c "usermod -a -G docker ${USER}"

docker-compose build
