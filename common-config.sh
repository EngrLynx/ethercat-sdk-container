#! /bin/bash

if [[ $_ == $0 ]]; then
    echo "Script needs to be sourced"
    exit 1
fi

export IMAGE_NAME="am437x-rt-dev"
export CONT_NAME="am437x-rt-dev1"

export SERIAL_DEVICE=""

export TGTFS_NAME="buildroot-2016.02"

export SDK_REL_PATH="sdk"
export GENFS_REL_PATH="genfs"
export SRC_REL_PATH="src"
export OUT_REL_PATH="out"
