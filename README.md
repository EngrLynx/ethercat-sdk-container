## Rationale:

1. Portability - Using Ubuntu 14.04 on top of Docker means that the SDK environment may be deployed on any platform supporting modern Docker. I say modern because the docker-io deb package of Ubuntu is too old and buggy such that it doesn't work with docker-compose which this system uses. As such, you don't need to install Ubuntu 14.04 on your system just to do development. In fact in theory, this system may even be deployed on iOS and Windows 10 (though I haven't tested this assertion).
2. Infrastructure as Code - Having an SDK on top of a container means that all platform requirements are put into code. Hence, it can be saved in a repo and put under version control.
3. Convenience - Since the development environment is inside a container, it can run inside your host platform which contains your preferred tools (IDE, version control, and whatever editor you want). So your development tools are all in your preferred OS whereas your tool-chain is in the container.

***

## Prerequisites:

1. Linux compatible with the new Docker - i.e. able to run `apt install docker` and **not** `apt install docker.io`
2. an internet connection since some items that are automatically installed will come from the internet
3. folder where TI Processor Linux RT SDK v2.00.01.07 was installed - its name should be the same as the environment variable SDK_REL_PATH specified in common-config.sh (See **How To Use** section below.)
4. tar.gz of buildroot - its name should be the same as the environment variable TGTFS_NAME specified in common-config.sh (See **How To Use** section below.)

***

## Provided Generic Components:
These are files written to automate the building and running of an SDK container. It should provide a portable and easy-to-use development environment. They are either Linux shell scripts, Docker files or patch files. Some of them have the purpose of creating and running Docker containers. While, most modify the basic software components provided by the TI SDK and by buildoot (u-boot, kernel, and rootfs) to be adapted to our configuration and hardware.

1. *cont-scripts/entry.sh* - executed everytime the SDK container is started; on first run, it will take a while to execute as it prepares the environment
2. *cont-script/pre-setup.sh* - modifies the setup scripts of the SDK to automatically configure the environment
3. *make-upd/u-boot.upd* - modifications for the u-boot Makefile to provide a deploy rule
4. *make-upd/kernel.upd* - modifications for the kernel Makefile to provide a deploy rule
5. *make-upd/rootfs.upd* - modifications for the rootfs Makefile to provide a deploy rule
6. *src/changes/u-boot.patch* - patch file that modifies the base u-boot code to adapt to our configuration and hardware
7. *src/changes/kernel/** - files modified from the base kernel code to adapt to our configuration and hardware
8. *src/changes/rootfs/** - files modified from the base rootfs code to adapt to our configuration and hardware
9. *src/make-u-boot.sh* - script that configures, builds and deploys the u-boot
10. *src/make-kernel.sh* - script that configures, builds and deploys the kernel
11. *src/make-rootfs.sh* - script that configures, builds and deploys the rootfs
12. *src/update.sh* - script that updates the u-boot, kernel, and rootfs source as per the necessary configuration
13. *Dockerfile* - Docker file providing the sequence to create the SDK container image
14. *README.md* - this guide
15. *build-config.sh* - environment variables specific for building the SDK container image
16. *build.sh* - script for building the SDK container image
17. *common-config.sh* - environment variables common to both building and running the SDK container
18. *docker-compose.yml* - Docker Compose configuration for creating and running the SDK container image
19. *run-config.sh* - environment variables specific for running the SDK container
20. *run.sh* - script for running the SDK container

***

## Relevant Folders:

1. `out` - this is where all the deployed binaries are placed; it's also the TFTP root
2. `src` - this is where all the source code should reside; **it's where developed software code should be placed**; it also includes code provided by the TI SDK and buildroot (already modified to adapt to our configuration and hardware)

***

## How To Use:

1. Download this repo by running `git clone git@bitbucket.org:EngrLynx/ethercat-sdk-container.git`.
2. Download the [TI Processor Linux RT SDK Installation Binary v2.00.01.07](http://software-dl.ti.com/processor-sdk-linux-rt/esd/AM437X/02_00_01_07/exports/ti-processor-sdk-linux-rt-am437x-evm-02.00.01.07-Linux-x86-Install.bin). Run it and install the SDK into the same location as where this repo was downloaded.
3. Download [Buildroot v2016-02](https://buildroot.org/downloads/buildroot-2016.02.tar.gz) into the same location as where this repo was downloaded.
4. From the command line, run `./build.sh`. **It will take a while since the SDK is huge and setup is done automatically.** However, this only needs to be done once.
5. Again from the command line, run `./run.sh`. This is what needs to be executed every time you want to get to the development environment. **The first time this is run, it will take a while** as it transfers items from the SDK container image into the `out` and `src` folders. This is done so that you can access the deployed binaries and the source code even outside the SDK container.
6. There is a `src` folder here where other source codes should be placed. For instance, xenomai and the test codes for eCPU driver should be located there.
7. While inside the SDK container and in the `src` folder, run `./make-u-boot.sh` to build u-boot. The output binaries will show up in the `out` folder.
8. While inside the SDK container and in the `src` folder, run `./make-kernel.sh` to build the kernel. The output binaries will show up in the `out` folder.
9. While inside the SDK container and in the `src` folder, run `./make-rootfs.sh` to build the filesystem. The output binaries will show up in the `out` folder.
