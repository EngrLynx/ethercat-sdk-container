#! /bin/bash

if [[ $_ == $0 ]]; then
    echo "Script needs to be sourced"
    exit 1
fi

export USER=`whoami`
export USER_ID=`id -u $USER`
export GROUP_ID=`id -g $USER`
export DEFAULT_PASSWD="ethercat"
