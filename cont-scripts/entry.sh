#! /bin/bash

if [ -d "/home/dev/temp" ]; then
    if [ ! -d "/home/dev/${OUT_REL_PATH}/pre-built" ]; then
        cp -rn /home/dev/temp/${OUT_REL_PATH}/pre-built /home/dev/${OUT_REL_PATH}
    fi
    if [ ! -d "/home/dev/${SRC_REL_PATH}/kernel" ]; then
        cp -rn /home/dev/temp/${SRC_REL_PATH}/kernel /home/dev/${SRC_REL_PATH}
    fi
    if [ ! -d "/home/dev/${SRC_REL_PATH}/u-boot" ]; then
        cp -rn /home/dev/temp/${SRC_REL_PATH}/u-boot /home/dev/${SRC_REL_PATH}
    fi
    /home/dev/${SRC_REL_PATH}/update.sh
    rm -rf /home/dev/temp
fi

sed -i "s=serverip .*\"$=serverip ${IP_ADDRESS}\"=g" /home/dev/${SDK_REL_PATH}/bin/setupBoard.minicom
