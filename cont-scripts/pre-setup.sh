#! /bin/bash

cwd=/home/dev/${SDK_REL_PATH}/bin
sed -i "s=TI_SDK_PATH\=.*$=TI_SDK_PATH\=/home/dev/${SDK_REL_PATH}=g" $cwd/../Rules.make
sed -i "s=dstdefault\=.*$=dstdefault\=/home/dev/${GENFS_REL_PATH}=g" $cwd/setup-targetfs-nfs.sh
sed -i "s=tftprootdefault\=.*$=tftprootdefault\=/home/dev/${OUT_REL_PATH}=g" $cwd/setup-tftp.sh
sed -i "s=portdefault\=.*$=portdefault\=/dev/ttyUSB0=g" $cwd/setup-minicom.sh
sed -i "s=minicomsetup\=.*$=minicomsetup\=\"n\"=g" $cwd/setup-uboot-env.sh
