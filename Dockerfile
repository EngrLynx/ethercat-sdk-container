# Pull Ubuntu 14.04 image as it is what's supported by the SDK
FROM ubuntu:14.04

# Set exposed port for TFTP
EXPOSE 69/udp

# Install SDK prerequisites and do clean up of apt repo
RUN apt-get update && apt-get install -y \
    xinetd \
    tftpd \
    nfs-kernel-server \
    minicom \
    build-essential \
    libncurses5-dev \
    autoconf \
    automake \
    dos2unix \
    u-boot-tools \
    device-tree-compiler \
    bc \
    wget \
    unzip \
    rsync \
    && apt-get clean

# Add would be location of SDK bin to PATH environment variable
ARG SDK_REL_PATH
ENV PATH /home/dev/${SDK_REL_PATH}/linux-devkit/sysroots/x86_64-arago-linux/usr/bin:$PATH

# Create container user w/ home directory to represent host user
ARG USER_ID
ARG GROUP_ID
ARG DEFAULT_PASSWD
RUN groupadd -r -g ${GROUP_ID} dev && \
    useradd --no-log-init -rm -u ${USER_ID} -g ${GROUP_ID} dev && \
    adduser dev sudo && \
    echo "dev:${DEFAULT_PASSWD}" | chpasswd && \
    chage -d 0 dev && \
    echo "dev ALL = NOPASSWD: /home/dev/${SDK_REL_PATH}/setup.sh, /bin/rm" >> /etc/sudoers
USER dev
WORKDIR /home/dev

# Create requisite folders in the home directory
ARG OUT_REL_PATH
ARG SRC_REL_PATH
RUN mkdir -p ./temp/${OUT_REL_PATH}/pre-built ./temp/${SRC_REL_PATH}/kernel ./temp/${SRC_REL_PATH}/u-boot

# Copy over the SDK to the image and set it up (args are needed by pre-setup.sh)
ARG GENFS_REL_PATH
ADD ${SDK_REL_PATH}.tar.gz /home/dev
COPY cont-scripts/pre-setup.sh /home/dev
RUN ./pre-setup.sh && \
    rm -f ./pre-setup.sh && \
    sudo ./${SDK_REL_PATH}/setup.sh

# Delete generated target fs and move board-support and tftp files to /home/dev/temp
RUN sudo rm -rf ./${GENFS_REL_PATH} && \
    cp -rf ./${OUT_REL_PATH}/* ./temp/${OUT_REL_PATH}/pre-built && \
    rm -rf ./${OUT_REL_PATH} && \
    cp -rf ./${SDK_REL_PATH}/board-support/linux*/* ./temp/${SRC_REL_PATH}/kernel && \
    rm -rf ./${SDK_REL_PATH}/board-support/linux* && \
    cp -rf ./${SDK_REL_PATH}/board-support/u-boot*/* ./temp/${SRC_REL_PATH}/u-boot && \
    rm -rf ./${SDK_REL_PATH}/board-support/u-boot*
VOLUME /home/dev/${SDK_REL_PATH}

# Default command includes container initialization script
COPY cont-scripts/entry.sh /usr/local/bin
CMD bash -C "entry.sh"; bash
