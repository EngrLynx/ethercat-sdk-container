#! /bin/bash

if [[ $_ == $0 ]]; then
    echo "Script needs to be sourced"
    exit 1
fi

echo "Which serial device is used by the board?"
select SERIAL_DEVICE in `ls /dev/ttyUSB*`; do
    break
done

echo "Which ip address is used by TFTP?"
select IP_ADDRESS in `ifconfig | grep "inet addr" | cut -d: -f2 | cut -d" " -f1`; do
    break
done

export SERIAL_DEVICE
export IP_ADDRESS

export COMPOSE_HTTP_TIMEOUT=360
