#! /bin/bash

export CROSS_COMPILE=arm-linux-gnueabihf-
export CWD=`pwd`
export MAKEDIR=/home/dev/${SRC_REL_PATH}/rootfs
export DESTDIR=${MAKEDIR}/output/target

if ! ${CROSS_COMPILE}gcc --version 2>/dev/null; then
    echo "Cross compiler not found. Make sure you are inside the SDK container."
    exit 1
fi

if [ ! -d "${MAKEDIR}" ]; then
    echo "Directory to be built ${MAKEDIR} not found."
    exit 1
fi

# Make and deploy rootfs image
cd ${MAKEDIR}
make am437x_ethercat_defconfig
make
make deploy
cd ${CWD}
echo "Build successful."
