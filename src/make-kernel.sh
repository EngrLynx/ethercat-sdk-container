#! /bin/bash

export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export CWD=`pwd`
export MAKEDIR=/home/dev/${SRC_REL_PATH}/kernel
export LOADADDR=0x82000000

if ! ${CROSS_COMPILE}gcc --version 2>/dev/null; then
    echo "Cross compiler not found. Make sure you are inside the SDK container."
    exit 1
fi

if [ ! -d "${MAKEDIR}" ]; then
    echo "Directory to be built ${MAKEDIR} not found."
    exit 1
fi

# Make and deploy Linux kernel
cd ${MAKEDIR}
make am437x_ethercat_defconfig
make uImage
make am437x-ethercat.dtb
make deploy
cd ${CWD}
echo "Build successful."
