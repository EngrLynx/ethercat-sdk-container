#include <asm/io.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/kfifo.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/spinlock_types.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/wait.h>

#define DRIVER_NAME         "ecpu"
#define ECPU_CLASS          "fpga"
#define ECPU_COMPAT         "arkus,ecpu"
#define ECPU_SIZE           12
#define ECPU_FIFO_SIZE      128

MODULE_ALIAS("platform:" DRIVER_NAME);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Leon Carlo Valencia <leoncarlo.valencia@arkus.ph>");
MODULE_DESCRIPTION("eCPU Character Device Driver");
MODULE_VERSION("0.1");

struct ecpu_data {
    unsigned long phys_base;
    unsigned int size;

    void __iomem *base;

    dev_t devno;
    struct cdev cdev;
    struct class *cls;

/*
    DECLARE_KFIFO(in_fifo, char, ECPU_FIFO_SIZE);
    DECLARE_KFIFO(out_fifo, char, ECPU_FIFO_SIZE);
    spinlock_t fifo_lock;
    wait_queue_head_t readable, writeable;
    struct mutex read_lock;
    struct mutex write_lock;
*/
};

static int     ecpu_open(struct inode *, struct file *);
static int     ecpu_release(struct inode *, struct file *);
static ssize_t ecpu_read(struct file *, char *, size_t, loff_t *);
static ssize_t ecpu_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops[] = {
    {
        .owner      = THIS_MODULE,
        .open       = ecpu_open,
        .release    = ecpu_release,
        .read       = ecpu_read,
        .write      = ecpu_write,
    },
};

static int ecpu_open(struct inode *inodep, struct file *filep)
{
    struct ecpu_data *drvdata = container_of(inodep->i_cdev, struct ecpu_data, cdev);
    filep->private_data = drvdata;
    return 0;
}
 
static int ecpu_release(struct inode *inodep, struct file *filep){
    filep->private_data = NULL;
    return 0;
}

static ssize_t ecpu_read(struct file *filep, char __user *buffer, size_t len, loff_t *offset){
    struct ecpu_data *drvdata = filep->private_data;

    if (*offset < 0 || *offset > ECPU_SIZE - 2 || *offset + len > ECPU_SIZE) {
        pr_err("Trying to read beyond valid addresses.\n");
        return -EFAULT;
    }

    if (len % 2 || *offset % 2) {
        pr_err("Trying to read partially from a register (each is 2-bytes wide).\n");
        return -EFAULT;
    }

    ioread16_rep(drvdata->base + *offset, buffer, len / 2);
    mb();
    *offset += len;
    return len;
}
 
static ssize_t ecpu_write(struct file *filep, const char __user *buffer, size_t len, loff_t *offset){
    struct ecpu_data *drvdata = filep->private_data;

    if (*offset < 4 || *offset > ECPU_SIZE - 2 || *offset + len > ECPU_SIZE) {
        pr_err("Trying to write beyond valid addresses.\n");
        return -EFAULT;
    }

    if (len % 2 || *offset % 2) {
        pr_err("Trying to write partially to a register (each is 2-bytes wide).\n");
        return -EFAULT;
    }

    iowrite16_rep(drvdata->base + *offset, buffer, len / 2);
    mmiowb();
    *offset += len;
    return len;
}
 
static struct ecpu_data *ecpu_data_init(void)
{
    struct ecpu_data *drvdata;

    drvdata = kzalloc(sizeof(*drvdata), GFP_KERNEL);
    if (!drvdata)
        return NULL;
/*
    init_waitqueue_head(&drvdata->readable);
    init_waitqueue_head(&drvdata->writeable);
    INIT_KFIFO(drvdata->in_fifo);
    INIT_KFIFO(drvdata->out_fifo);
    mutex_init(&drvdata->read_lock);
    mutex_init(&drvdata->write_lock);
*/
    return drvdata;
}

static int ecpu_probe(struct platform_device *pdev)
{
    struct ecpu_data *drvdata;
    struct resource *res;
    int ret;

    drvdata = ecpu_data_init();
    if (!drvdata) {
        ret = -ENOMEM;
        pr_err("Driver data initialization failed.\n");
        goto exit;
    }

    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (res == NULL) {
        ret = -EINVAL;
        pr_err("Error getting memory resource.\n");
        goto exit_alloc;
    }
    drvdata->phys_base = res->start;
    drvdata->size = resource_size(res);

    if (request_mem_region(drvdata->phys_base, drvdata->size,
            KBUILD_MODNAME) == NULL) {
        pr_err("Cannot reserve memory region at 0x%08lx, size: 0x%x\n",
            drvdata->phys_base, drvdata->size);
        ret = -EBUSY;
        goto exit_alloc;
    }

    drvdata->base = ioremap_nocache(drvdata->phys_base, drvdata->size);
    if (drvdata->base == NULL) {
        ret = -ENOMEM;
        pr_err("Unable to map device memory.\n");
        goto exit_region;
    }

    ret = alloc_chrdev_region(&drvdata->devno, 0, 1, KBUILD_MODNAME);
    if (ret < 0) {
        pr_err("Error %d allocating device number.\n", ret);
        goto exit_map;
    }

    drvdata->cls = class_create(THIS_MODULE, ECPU_CLASS);
    if (drvdata->cls == NULL) {
        ret = -ENOMEM;
        pr_err("Unable to create device class.\n");
        goto exit_devno;
    }

    if(device_create(drvdata->cls, NULL, drvdata->devno, NULL, KBUILD_MODNAME) == NULL) {
        ret = -ENOMEM;
        pr_err("Unable create device node.\n");
        goto exit_class;
    }

    cdev_init(&drvdata->cdev, fops);
    ret = cdev_add(&drvdata->cdev, drvdata->devno, 1);
    if (ret < 0) {
        pr_err("Error %d registering char device.\n", ret);
        goto exit_device;
    }

    return 0;

exit_device:
    device_destroy(drvdata->cls, drvdata->devno);
exit_class:
    class_destroy(drvdata->cls);
exit_devno:
    unregister_chrdev_region(drvdata->devno, 1);
exit_map:
    iounmap(drvdata->base);
exit_region:
    release_mem_region(drvdata->phys_base, drvdata->size);
exit_alloc:
    kfree(drvdata);
exit:
    return ret;
}

static void ecpu_shutdown(struct platform_device *pdev)
{
    // struct ecpu_data *drvdata = dev_get_drvdata(&pdev->dev);
    /* Set data to desired value before reset. */
}

static int ecpu_remove(struct platform_device *pdev)
{
    struct ecpu_data *drvdata = dev_get_drvdata(&pdev->dev);

    cdev_del(&drvdata->cdev);
    device_destroy(drvdata->cls, drvdata->devno);
    class_destroy(drvdata->cls);
    unregister_chrdev_region(drvdata->devno, 1);

    ecpu_shutdown(pdev);
    iounmap(drvdata->base);
    release_mem_region(drvdata->phys_base, drvdata->size);
    kfree(drvdata);

    return 0;
}

static const struct of_device_id of_ecpu_platform_device_match[] = {
    { .compatible = ECPU_COMPAT, },
    {},
};

MODULE_DEVICE_TABLE(of, of_ecpu_platform_device_match);

static struct platform_driver ecpu_platform_driver = {
    .probe      = ecpu_probe,
    .remove     = ecpu_remove,
    .shutdown   = ecpu_shutdown,
    .driver     = {
        .name   = KBUILD_MODNAME,
        .of_match_table = of_ecpu_platform_device_match,
        .owner = THIS_MODULE,
    },
};

module_platform_driver(ecpu_platform_driver);
