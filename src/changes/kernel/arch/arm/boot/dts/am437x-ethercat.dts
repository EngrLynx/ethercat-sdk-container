/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;

#include "am4372.dtsi"
#include <dt-bindings/pinctrl/am43xx.h>
#include <dt-bindings/pwm/pwm.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>

/ {
	model = "TI AM437x EtherCAT";
	compatible = "ti,am437x-idk-evm","ti,am4372","ti,am43";

	v24_0d: fixed-regulator-v24_0d {
		compatible = "regulator-fixed";
		regulator-name = "V24_0D";
		regulator-min-microvolt = <24000000>;
		regulator-max-microvolt = <24000000>;
		regulator-always-on;
		regulator-boot-on;
	};

	v3_3d: fixed-regulator-v3_3d {
		compatible = "regulator-fixed";
		regulator-name = "V3_3D";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v24_0d>;
	};

	vdd_corereg: fixed-regulator-vdd_corereg {
		compatible = "regulator-fixed";
		regulator-name = "VDD_COREREG";
		regulator-min-microvolt = <1100000>;
		regulator-max-microvolt = <1100000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v24_0d>;
	};

	vdd_core: fixed-regulator-vdd_core {
		compatible = "regulator-fixed";
		regulator-name = "VDD_CORE";
		regulator-min-microvolt = <1100000>;
		regulator-max-microvolt = <1100000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&vdd_corereg>;
	};

	v1_8dreg: fixed-regulator-v1_8dreg{
		compatible = "regulator-fixed";
		regulator-name = "V1_8DREG";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v24_0d>;
	};

	v1_8d: fixed-regulator-v1_8d{
		compatible = "regulator-fixed";
		regulator-name = "V1_8D";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v1_8dreg>;
	};

	v1_5dreg: fixed-regulator-v1_5dreg{
		compatible = "regulator-fixed";
		regulator-name = "V1_5DREG";
		regulator-min-microvolt = <1500000>;
		regulator-max-microvolt = <1500000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v24_0d>;
	};

	v1_5d: fixed-regulator-v1_5d{
		compatible = "regulator-fixed";
		regulator-name = "V1_5D";
		regulator-min-microvolt = <1500000>;
		regulator-max-microvolt = <1500000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&v1_5dreg>;
	};

	leds {
		compatible = "gpio-leds";

		pinctrl-names = "default";
		pinctrl-0 = <&leds_pins>;

		led@0 {
			label = "stledr0";
			gpios = <&gpio5 1 GPIO_ACTIVE_HIGH>;	/* Bank 5, pin 1 */
			default-state = "on";
		};

		led@1 {
			label = "stledb0";
			gpios = <&gpio5 2 GPIO_ACTIVE_HIGH>;	/* Bank 5, pin 2 */
			default-state = "on";
		};

		led@2 {
			label = "almled";
			gpios = <&gpio5 3 GPIO_ACTIVE_HIGH>;	/* Bank 5, pin 3 */
			default-state = "on";
		};

		led@3 {
			label = "stledr1";
			gpios = <&gpio5 19 GPIO_ACTIVE_HIGH>;	/* Bank 5, pin 19 */
			default-state = "on";
		};

		led@4 {
			label = "stledb1";
			gpios = <&gpio5 20 GPIO_ACTIVE_HIGH>;	/* Bank 5, pin 20 */
			default-state = "on";
		};
	};
};

&am43xx_pinmux {
	leds_pins: leds_pins {
		pinctrl-single,pins = <
			0x234 (PIN_OUTPUT | MUX_MODE7)	/* uart3_rtsn.gpio5_1 */
			0x228 (PIN_OUTPUT | MUX_MODE7)	/* uart3_rxd.gpio5_2 */
			0x22c (PIN_OUTPUT | MUX_MODE7)	/* uart3_txd.gpio5_3 */
			0x208 (PIN_OUTPUT | MUX_MODE7)	/* cam0_data0.gpio5_19 */
			0x20c (PIN_OUTPUT | MUX_MODE7)	/* cam0_data1.gpio5_20 */
		>;
	};


	gpio0_pins_default: gpio0_pins_default {
		pinctrl-single,pins = <
/*pcb version 01a*/
			/*0xD0 (PIN_INPUT | MUX_MODE7)*/	/* dss_data12.gpio0_8 */
			/*0x178 (PIN_INPUT | MUX_MODE7)*/	/* uart1_ctsn.gpio0_12 */
			/*0x1A8 (PIN_INPUT | MUX_MODE9)*/	/* mcasp0_axr1.gpio0_2 */
			/*0x1AC (PIN_INPUT | MUX_MODE9)*/	/* mcasp0_ahclkx.gpio0_3 */
/*pcb version 01b*/
			/*0xD0 (PIN_INPUT | MUX_MODE7)*/	/* DSW1-4 dss_data12.gpio0_8 */
			/*0x178 (PIN_INPUT | MUX_MODE7)*/	/* DSW1-8 uart1_ctsn.gpio0_12 (Reg 978, offset 0x800)*/
			/*0x17C (PIN_INPUT | MUX_MODE7)*/	/* DSW2-1 uart1_rtsn.GPIO0_13 (REG.97c)*/
			/*0x180 (PIN_INPUT | MUX_MODE7)*/	/* DSW2-2 uart1_rxd.GPIO0_14 (REG.980)*/
			/*0x184 (PIN_INPUT | MUX_MODE7)*/	/* DSW2-4 uart1_txd.GPIO0_15 (REG.984)*/
			/*0x1A8 (PIN_INPUT | MUX_MODE9)*/	/* DSW1-1 mcasp0_axr1.gpio0_2 (Reg 9A8, offset 0x800) */
			/*0x1AC (PIN_INPUT | MUX_MODE9)*/	/* DSW1-2 mcasp0_ahclkx.gpio0_3 (Reg 9AC, offset 0x800) */
			/*0x260 (PIN_INPUT | MUX_MODE7)*/	/* DSW3-4 spi2_sclk.GPIO0_22 (REG A60) */
			/*0x268 (PIN_INPUT | MUX_MODE7)*/	/* DSW3-2 spi2_d1.GPIO0_21 (REG.A68) */
			/*0x26C (PIN_INPUT | MUX_MODE7)*/	/* DSW3-8 spi2_cs0.GPIO0_23 REG.A6C */
			/*0x274 (PIN_INPUT | MUX_MODE7)*/	/* DSW3-1 xdma_evt_intr1.GPIO0_20 (REG.A74) */
			/*0x2C0 (PIN_INPUT | MUX_MODE7)*/	/* DSW2-8 usb0_drvvbus.GPIO0_18 (REG.AC0) */
/*pcb version 01C*/
			0x1A8 (PIN_INPUT | MUX_MODE9)	/* DSW1-1 mcasp0_axr1.gpio0_2 (Reg 9A8, offset 0x800) */
			0x1AC (PIN_INPUT | MUX_MODE9)	/* DSW1-2 mcasp0_ahclkx.gpio0_3 (Reg 9AC, offset 0x800) */
			/*0xD0 (PIN_INPUT | MUX_MODE7)*/	/* DSW1-4 dss_data12.gpio0_8 */
			0x178 (PIN_INPUT | MUX_MODE7)	/* DSW1-4 uart1_ctsn.gpio0_12 (Reg 978, offset 0x800)*/
			0x17C (PIN_INPUT | MUX_MODE7)	/* DSW1-8 uart1_rtsn.GPIO0_13 (REG.97c)*/
			0x180 (PIN_INPUT | MUX_MODE7)	/* DSW2-1 uart1_rxd.GPIO0_14 (REG.980)*/
			0x184 (PIN_INPUT | MUX_MODE7)	/* DSW2-2 uart1_txd.GPIO0_15 (REG.984)*/
			0x2C0 (PIN_INPUT | MUX_MODE7)	/* DSW2-4 usb0_drvvbus.GPIO0_18 (REG.AC0) */			
			0x274 (PIN_INPUT | MUX_MODE7)	/* DSW2-8 xdma_evt_intr1.GPIO0_20 (REG.A74) */
			0x268 (PIN_INPUT | MUX_MODE7)	/* DSW3-1 spi2_d1.GPIO0_21 (REG.A68) */
			0x260 (PIN_INPUT | MUX_MODE7)	/* DSW3-2 spi2_sclk.GPIO0_22 (REG A60) */
			0x26C (PIN_INPUT | MUX_MODE7)	/* DSW3-4 spi2_cs0.GPIO0_23 REG.A6C */
			0x70 (PIN_INPUT | MUX_MODE7)	/* DSW3-4 gpmc_wait0.GPIO0_30 REG.870 */
		>;
	};

	gpio1_pins_default: gpio1_pins_default {
		pinctrl-single,pins = <
			0x168 (PIN_OUTPUT | MUX_MODE7)	/* uart0_ctsn.gpio1_8 */
			0x16C (PIN_OUTPUT | MUX_MODE7)	/* uart0_rtsn.gpio1_9 */
		>;
	};


	gpio2_pins_default: gpio2_pins_default {
		pinctrl-single,pins = <
/*pcb version 01A*/		
			/*0xB8 (PIN_INPUT | MUX_MODE7)*/	/* dss_data6.gpio2_12 */
			/*0xBC (PIN_INPUT | MUX_MODE7)*/	/* dss_data7.gpio2_13 */
			/*0xE4 (PIN_OUTPUT | MUX_MODE7)*/	/* dss_hsync.gpio2_23 */
			/*0xEC (PIN_OUTPUT | MUX_MODE7)*/	/* dss_ac_bias_en.gpio2_25 */
/*pcb version 01C*/			
			0x104 (PIN_OUTPUT | MUX_MODE7)	/* mmc0_cmd.GPIO2_31 CRESET */		
		>;
	};

	gpio4_pins_default: gpio4_pins_default {
		pinctrl-single,pins = <
			0x1B0 (PIN_INPUT | MUX_MODE7)	/* cam0_hd.gpio4_0 */
			0x1B4 (PIN_INPUT | MUX_MODE7)	/* cam0_vd.gpio4_1 */
			0x1B8 (PIN_INPUT | MUX_MODE7)	/* cam0_field.gpio4_2 */
			0x1BC (PIN_INPUT | MUX_MODE7)	/* cam0_wen.gpio4_3 */
			0x1C0 (PIN_INPUT | MUX_MODE7)	/* cam0_pclk.gpio4_4 */
			0x1C4 (PIN_INPUT | MUX_MODE7)	/* cam0_data8.gpio4_5 */
			0x1C8 (PIN_INPUT | MUX_MODE7)	/* cam0_data9.gpio4_6 */
			0x1CC (PIN_INPUT | MUX_MODE7)	/* cam1_data9.gpio4_7 */
			0x1D8 (PIN_INPUT | MUX_MODE7)	/* cam1_vd.gpio4_10 */
			0x1E8 (PIN_INPUT | MUX_MODE7)	/* cam1_data0.gpio4_14 */
			0x1EC (PIN_INPUT | MUX_MODE7)	/* cam1_data1.gpio4_15 */
			0x1F0 (PIN_INPUT | MUX_MODE7)	/* cam1_data2.gpio4_16 */
			0x1F4 (PIN_INPUT | MUX_MODE7)	/* cam1_data3.gpio4_17 */
			0x1F8 (PIN_INPUT | MUX_MODE7)	/* cam1_data4.gpio4_18 */
			0x1FC (PIN_INPUT | MUX_MODE7)	/* cam1_data5.gpio4_19 */
			0x200 (PIN_INPUT | MUX_MODE7)	/* cam1_data6.gpio4_20 */
		>;
	};

	gpio4_pins_default: gpio4_pins_default {
		pinctrl-single,pins = <
			0x1B0 (PIN_INPUT | MUX_MODE7)	/* cam0_hd.gpio4_0 */
			0x1B4 (PIN_INPUT | MUX_MODE7)	/* cam0_vd.gpio4_1 */
			0x1B8 (PIN_INPUT | MUX_MODE7)	/* cam0_field.gpio4_2 */
			0x1BC (PIN_INPUT | MUX_MODE7)	/* cam0_wen.gpio4_3 */
			0x1C0 (PIN_INPUT | MUX_MODE7)	/* cam0_pclk.gpio4_4 */
			0x1C4 (PIN_INPUT | MUX_MODE7)	/* cam0_data8.gpio4_5 */
			0x1C8 (PIN_INPUT | MUX_MODE7)	/* cam0_data9.gpio4_6 */
			0x1CC (PIN_INPUT | MUX_MODE7)	/* cam1_data9.gpio4_7 */
			0x1D8 (PIN_INPUT | MUX_MODE7)	/* cam1_vd.gpio4_10 */
			0x1E8 (PIN_INPUT | MUX_MODE7)	/* cam1_data0.gpio4_14 */
			0x1EC (PIN_INPUT | MUX_MODE7)	/* cam1_data1.gpio4_15 */
			0x1F0 (PIN_INPUT | MUX_MODE7)	/* cam1_data2.gpio4_16 */
			0x1F4 (PIN_INPUT | MUX_MODE7)	/* cam1_data3.gpio4_17 */
			0x1F8 (PIN_INPUT | MUX_MODE7)	/* cam1_data4.gpio4_18 */
			0x1FC (PIN_INPUT | MUX_MODE7)	/* cam1_data5.gpio4_19 */
			0x200 (PIN_INPUT | MUX_MODE7)	/* cam1_data6.gpio4_20 */
		>;
	};

	i2c0_pins_default: i2c0_pins_default {
		pinctrl-single,pins = <
			0x188 (PIN_INPUT | SLEWCTRL_FAST | MUX_MODE0) /* i2c0_sda.i2c0_sda */
			0x18c (PIN_INPUT | SLEWCTRL_FAST | MUX_MODE0) /* i2c0_scl.i2c0_scl */
		>;
	};

	i2c0_pins_sleep: i2c0_pins_sleep {
		pinctrl-single,pins = <
			0x188 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x18c (PIN_INPUT_PULLDOWN | MUX_MODE7)
		>;
	};

	i2c2_pins_default: i2c2_pins_default {
		pinctrl-single,pins = <
			0x1e8 (PIN_INPUT | SLEWCTRL_FAST | MUX_MODE3) /* cam1_data1.i2c2_scl */
			0x1ec (PIN_INPUT | SLEWCTRL_FAST | MUX_MODE3) /* cam1_data0.i2c2_sda */
		>;
	};

	i2c2_pins_sleep: i2c2_pins_sleep {
		pinctrl-single,pins = <
			0x1e8 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x1ec (PIN_INPUT_PULLDOWN | MUX_MODE7)
		>;
	};

	cpsw_default: cpsw_default {
		pinctrl-single,pins = <
			0x108 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_col */
			0x10c (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_crs */
			0x110 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxerr */
			0x114 (PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* mii1_txen */
			0x118 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxdv */
			0x11C (PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* mii1_txd3 */
			0x120 (PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* mii1_txd2 */
			0x124 (PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* mii1_txd1 */
			0x128 (PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* mii1_txd0 */
			0x12C (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_txclk */
			0x130 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxclk */
			0x134 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxd3 */
			0x138 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxd2 */
			0x13c (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxd1 */
			0x140 (PIN_INPUT_PULLDOWN | MUX_MODE0)	/* mii1_rxd0 */
		>;
	};

	cpsw_sleep: cpsw_sleep {
		pinctrl-single,pins = <
			0x108 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_col */
			0x10c (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_crs */
			0x110 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxerr */
			0x114 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txen */
			0x118 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxdv */
			0x11C (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd3 */
			0x120 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd2 */
			0x124 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd1 */
			0x128 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd0 */
			0x12C (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txclk */
			0x130 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxclk */
			0x134 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd3 */
			0x138 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd2 */
			0x13c (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd1 */
			0x140 (PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd0 */
		>;
	};

	davinci_mdio_default: davinci_mdio_default {
		pinctrl-single,pins = <
			/* MDIO */
			0x148 (PIN_INPUT_PULLUP | SLEWCTRL_FAST | MUX_MODE0)	/* mdio_data.mdio_data */
			0x14c (PIN_OUTPUT_PULLUP | MUX_MODE0)			/* mdio_clk.mdio_clk */
		>;
	};

	davinci_mdio_sleep: davinci_mdio_sleep {
		pinctrl-single,pins = <
			/* MDIO reset value */
			0x148 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x14c (PIN_INPUT_PULLDOWN | MUX_MODE7)
		>;
	};

	qspi_pins_default: qspi_pins_default {
		pinctrl-single,pins = <
			0x214 (PIN_OUTPUT_PULLUP | MUX_MODE3)	/* cam0_data3.qspi_csn */
			0x210 (PIN_OUTPUT_PULLUP | MUX_MODE3)	/* cam0_data2.qspi_clk */
			0x218 (PIN_INPUT_PULLUP | MUX_MODE3)	/* cam0_data4.qspi_d0 */
			0x21c (PIN_INPUT_PULLUP | MUX_MODE3)	/* cam0_data5.qspi_d1 */
			0x220 (PIN_INPUT_PULLUP | MUX_MODE3)	/* cam0_data6.qspi_d2 */
			0x224 (PIN_INPUT_PULLUP | MUX_MODE3)	/* cam0_data7.qspi_d3 */
		>;
	};

	qspi_pins_sleep: qspi_pins_sleep{
		pinctrl-single,pins = <
			0x214 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x210 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x218 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x21c (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x220 (PIN_INPUT_PULLDOWN | MUX_MODE7)
			0x224 (PIN_INPUT_PULLDOWN | MUX_MODE7)
		>;
	};

	spi0_pins: pinmux_spi0_pins {
		pinctrl-single,pins = <
			0x154 (PIN_OUTPUT | MUX_MODE0)           /* spi0_d0.spi0_d0 */
			0x158 (PIN_INPUT | MUX_MODE0)           /* spi0_d1.spi0_d1 */
			0x150 (PIN_INPUT | MUX_MODE0)           /* spi0_sclk.spi0_clk */
			0x15c (PIN_INPUT | MUX_MODE0)          /* spi0_cs0.spi0_cs0 */
			0x160 (PIN_OUTPUT | MUX_MODE0)          /* spi0_cs1.spi0_cs1 */
		>;
	};

	spi1_pins: pinmux_spi1_pins {
		pinctrl-single,pins = <
			0x190 (PIN_INPUT | MUX_MODE3)           /* mcasp0_aclkx.spi1_clk */
			0x194 (PIN_OUTPUT | MUX_MODE3)           /* mcasp0_fsx.spi1_d0 */
			0x198 (PIN_INPUT | MUX_MODE3)           /* mcasp0_axr0.spi1_d1 */
			0x144 (PIN_OUTPUT | MUX_MODE2)          /* rmii1_refclk.spi1_cs0 */
			0x164 (PIN_OUTPUT | MUX_MODE2)          /* ecap0_in_pwm0_out.spi1_cs0 */
		>;
	};

	spi2_pins: pinmux_spi2_pins {
		pinctrl-single,pins = <
			0x1E0 (PIN_OUTPUT | MUX_MODE4)          /* cam1_field.spi2_cs1 */
			0x1E4 (PIN_INPUT | MUX_MODE4)           /* cam1_wen.spi2_d1 */
			0x1DC (PIN_INPUT | MUX_MODE4)           /* cam1_pclk.spi2_clk */
			0x1D0 (PIN_OUTPUT | MUX_MODE4)           /* cam1_data8.spi2_d0 */
			0x1D4 (PIN_OUTPUT | MUX_MODE4)          /* cam1_hd.spi2_cs0 */
		>;
	};

	spi3_pins: pinmux_spi3_pins {
		pinctrl-single,pins = <
			0x1f0 (PIN_OUTPUT | MUX_MODE2)			/* spi3_d0 */
			0x1ec (PIN_INPUT | MUX_MODE2)			/* spi3_d1 */
			0x1f4 (PIN_INPUT | MUX_MODE2)			/* spi3_clk */
			0x1e8 (PIN_OUTPUT | MUX_MODE2)			/* spi3_cs0 */
			0x9c (PIN_OUTPUT | MUX_MODE6)          /* spi3_cs1 */
		>;
	};

	spi4_pins: pinmux_spi4_pins {
		pinctrl-single,pins = <
			0x250 (PIN_INPUT | MUX_MODE0)           /* spi4_sclk.spi4_clk */
			0x254 (PIN_OUTPUT | MUX_MODE0)           /* spi4_d0.spi4_d0 */
			0x258 (PIN_INPUT | MUX_MODE0)           /* spi4_d1.spi4_d1 */
			0x25c (PIN_OUTPUT | MUX_MODE0)          /* spi4_cs0.spi4_cs0 */
			0x230 (PIN_OUTPUT | MUX_MODE2)          /* 230 uart3_ctsn.spi4_cs0 */
		>;
	};

	mdio_pruss11_pins_default: mdio_pruss11_pins_default {
		pinctrl-single,pins = <
			0x8c ( PIN_OUTPUT | MUX_MODE5 ) /* (A12) gpmc_clk.pr1_mdio_mdclk */
			0x88 ( PIN_INPUT | MUX_MODE5 ) /* (B12) gpmc_csn3.pr1_mdio_data */
			0x58 ( PIN_INPUT | MUX_MODE5 ) /* (E8) gpmc_a6.pr1_mii_mt1_clk */
			0x54 ( PIN_OUTPUT | MUX_MODE5 ) /* (E7) gpmc_a5.pr1_mii1_txd0 */
			0x50 ( PIN_OUTPUT | MUX_MODE5 ) /* (D7) gpmc_a4.pr1_mii1_txd1 */
			0x4c ( PIN_OUTPUT | MUX_MODE5 ) /* (A4) gpmc_a3.pr1_mii1_txd2 */
			0x48 ( PIN_OUTPUT | MUX_MODE5 ) /* (C6) gpmc_a2.pr1_mii1_txd3 */
			0x6c ( PIN_INPUT | MUX_MODE5 ) /* (D8) gpmc_a11.pr1_mii1_rxd0 */
			0x68 ( PIN_INPUT | MUX_MODE5 ) /* (G8) gpmc_a10.pr1_mii1_rxd1 */
			0x64 ( PIN_INPUT | MUX_MODE5 ) /* (B4) gpmc_a9.pr1_mii1_rxd2 */
			0x60 ( PIN_INPUT | MUX_MODE5 ) /* (F7) gpmc_a8.pr1_mii1_rxd3 */
			0x40 ( PIN_OUTPUT | MUX_MODE5 ) /* (C3) gpmc_a0.pr1_mii1_txen */
			0x5c ( PIN_INPUT | MUX_MODE5 ) /* (F6) gpmc_a7.pr1_mii_mr1_clk */
			0x44 ( PIN_INPUT| MUX_MODE5 ) /* (C5) gpmc_a1.pr1_mii1_rxdv */
			0x74 ( PIN_INPUT | MUX_MODE5 ) /* (B3) gpmc_wpn.pr1_mii1_rxer */
			0x24c ( PIN_INPUT | MUX_MODE5 ) /* (E24) gpio5_13.pr1_mii1_rxlink */
			0x244 ( PIN_INPUT | MUX_MODE5 ) /* (F23) gpio5_11.pr1_mii1_crs */
			0x23c ( PIN_INPUT | MUX_MODE5 ) /* (F24) gpio5_9.pr1_mii1_col */
			0xa0 ( PIN_INPUT | MUX_MODE2 ) /* (B22) dss_data0.pr1_mii_mt0_clk */
			0xb4 ( PIN_OUTPUT | MUX_MODE2 ) /* (B20) dss_data5.pr1_mii0_txd0 */
			0xb0 ( PIN_OUTPUT | MUX_MODE2 ) /* (A20) dss_data4.pr1_mii0_txd1 */
			0xac ( PIN_OUTPUT | MUX_MODE2 ) /* (C21) dss_data3.pr1_mii0_txd2 */
			0xa8 ( PIN_OUTPUT | MUX_MODE2 ) /* (B21) dss_data2.pr1_mii0_txd3 */
			0xcc ( PIN_INPUT | MUX_MODE5 ) /* (B18) dss_data11.pr1_mii0_rxd0 */
			0xc8 ( PIN_INPUT | MUX_MODE5 ) /* (A18) dss_data10.pr1_mii0_rxd1 */
			0xc4 ( PIN_INPUT | MUX_MODE5 ) /* (B19) dss_data9.pr1_mii0_rxd2 */
			0xc0 ( PIN_INPUT | MUX_MODE5 ) /* (A19) dss_data8.pr1_mii0_rxd3 */
			0xa4 ( PIN_OUTPUT | MUX_MODE2 ) /* (A21) dss_data1.pr1_mii0_txen */
			0xd8 ( PIN_INPUT | MUX_MODE5 ) /* (C17) dss_data14.pr1_mii_mr0_clk */
			0xdc ( PIN_INPUT | MUX_MODE5 ) /* (D17) dss_data15.pr1_mii0_rxdv */
			0xd4 ( PIN_INPUT | MUX_MODE5 ) /* (D19) dss_data13.pr1_mii0_rxer */
			0x240 ( PIN_INPUT | MUX_MODE5 ) /* (G20) gpio5_10.pr1_mii0_crs */
			0x238 ( PIN_INPUT | MUX_MODE5 ) /* (D25) gpio5_8.pr1_mii0_col */
/*pcbver01b*/
			/*0x248 ( PIN_INPUT | MUX_MODE5 )*/ /* (E25) gpio5_12.pr1_mii0_rxlink */
/*pcbver01c*/
			0xD0 ( PIN_INPUT | MUX_MODE5 ) /* (C19) dss_data12.pr1_mii0_rxlink REG 8D0h */
		>;
	};

	timer4_pins: pinmux_timer4_pins {
		pinctrl-single,pins = <
			0x270 (PIN_OUTPUT | MUX_MODE2)           /* xdma_event_intr0.timer4 */
		>;
	};

/*pcb eCPU1 version 01B*/
	gpmc_pins: gpmc_pins {
		pinctrl-single,pins = <
			0x00 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad0.gpmc_ad0 */
			0x04 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad1.gpmc_ad1 */
			0x08 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad2.gpmc_ad2 */
			0x0c (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad3.gpmc_ad3 */
			0x10 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad4.gpmc_ad4 */
			0x14 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad5.gpmc_ad5 */
			0x18 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad6.gpmc_ad6 */
			0x1c (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad7.gpmc_ad7 */
			0x20 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad8.gpmc_ad8 */
			0x24 (PIN_INPUT_PULLUP | MUX_MODE0) /* gpmc_ad9.gpmc_ad9 */
			0x28 (PIN_INPUT_PULLUP | MUX_MODE0) /* gpmc_ad10.gpmc_ad10 */
			0x2c (PIN_INPUT_PULLUP | MUX_MODE0) /* gpmc_ad11.gpmc_ad11 */
			0x30 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad12.gpmc_ad12 */
			0x34 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad13.gpmc_ad13 */
			0x38 (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad14.gpmc_ad14 */
			0x3c (PIN_INPUT_PULLUP | MUX_MODE0)	/* gpmc_ad15.gpmc_ad15 */

			0x7c (PIN_OUTPUT | MUX_MODE0)	/* gpmc_csn0.gpio1_29 */
			0x90 (PIN_OUTPUT | MUX_MODE0)	/* gpmc_advn_ale.gpmc_advn_ale */
			0x94 (PIN_OUTPUT | MUX_MODE0)	/* gpmc_oen_ren.gpmc_oen_ren */
			0x98 (PIN_OUTPUT | MUX_MODE0)	/* gpmc_wen.gpmc_wen */
		>;
	};

};

&i2c0 {
	status = "okay";
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c0_pins_default>;
	pinctrl-1 = <&i2c0_pins_sleep>;
	clock-frequency = <400000>;
/*
	at24@50 {
		compatible = "at24,24c256";
		pagesize = <64>;
		reg = <0x50>;
	};*/

	tps65218: tps65218@24 {
		reg = <0x24>;
		compatible = "ti,tps65218";
		interrupts = <GIC_SPI 7 IRQ_TYPE_NONE>; /* NMIn */
		interrupt-controller;
		#interrupt-cells = <2>;

		dcdc1: regulator-dcdc1 {
			compatible = "ti,tps65218-dcdc1";
			regulator-name = "vdd_core";
			regulator-min-microvolt = <912000>;
			regulator-max-microvolt = <1144000>;
			regulator-boot-on;
			regulator-always-on;
		};

		dcdc2: regulator-dcdc2 {
			compatible = "ti,tps65218-dcdc2";
			regulator-name = "vdd_mpu";
			regulator-min-microvolt = <912000>;
			regulator-max-microvolt = <1378000>;
			regulator-boot-on;
			regulator-always-on;
		};

		dcdc3: regulator-dcdc3 {
			compatible = "ti,tps65218-dcdc3";
			regulator-name = "vdcdc3";
			regulator-min-microvolt = <1500000>;
			regulator-max-microvolt = <1500000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-state-mem {
				regulator-on-in-suspend;
			};
			regulator-state-disk {
				regulator-off-in-suspend;
			};
		};

		dcdc5: regulator-dcdc5 {
			compatible = "ti,tps65218-dcdc5";
			regulator-name = "v1_0bat";
			regulator-min-microvolt = <1000000>;
			regulator-max-microvolt = <1000000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-state-mem {
				regulator-on-in-suspend;
			};
		};

		dcdc6: regulator-dcdc6 {
			compatible = "ti,tps65218-dcdc6";
			regulator-name = "v1_8bat";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-state-mem {
				regulator-on-in-suspend;
			};
		};

		ldo1: regulator-ldo1 {
			compatible = "ti,tps65218-ldo1";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-boot-on;
			regulator-always-on;
		};
	};
};

&gpio0 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&gpio0_pins_default>;
};

&gpio1 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&gpio1_pins_default>;
};

&gpio2 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&gpio2_pins_default>;
/*
	p25 {
		gpio-hog;
		gpios = <25 GPIO_ACTIVE_HIGH>;
		output-high;
		line-name = "spi-creset";
	};
*/
};

&gpio4 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&gpio4_pins_default>;
};

&gpio5 {
	status = "okay";
};

&spi1 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi1_pins>;
	status = "okay";
	ti,spi-num-cs = <2>;
	dmas = <&edma 42 &edma 43 &edma 44 &edma 45>;
	dma-names = "tx0", "rx0", "tx1", "rx1";
	ti,pindir-d0-out-d1-in = <1>;

	spi1_0@0x00 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <0>;
	};
	spi1_1@0x01 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <1>;
	};
};


&spi2 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi2_pins>;
	status = "okay";
	ti,spi-num-cs = <2>;
	dmas = <&edma 38 &edma 39 &edma 40 &edma 41>;
	dma-names = "tx0", "rx0", "tx1", "rx1";
	ti,pindir-d0-out-d1-in = <1>;
	spi2_0@0x00 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <0>;
	};
	spi2_1@0x01 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <1>;
	};
};

&spi3 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi3_pins>;
	status = "okay";
	ti,spi-num-cs = <2>;
	dmas = <&edma 53 &edma 54 &edma 55 &edma 56>;
	dma-names = "tx0", "rx0", "tx1", "rx1";
	ti,pindir-d0-out-d1-in = <1>;
	spi3_0@0x00 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <0>;
	};
	spi3_1@0x01 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <1>;
	};
};

&spi4 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi4_pins>;
	status = "okay";
	ti,spi-num-cs = <2>;
	dmas = <&edma 4 &edma 7 &edma 14 &edma 15>;
	dma-names = "tx0", "rx0", "tx1", "rx1";
	ti,pindir-d0-out-d1-in = <1>;
	spi4_0@0x00 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <0>;
	};
	spi4_1@0x01 {
		compatible = "rohm,dh2228fv";
		spi-max-frequency = <20000000>;
		reg = <1>;
	};
};

&epwmss0 {
	status = "okay";
};

/*
&qspi {
	status = "okay";
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&qspi_pins_default>;
	pinctrl-1 = <&qspi_pins_sleep>;

	spi-max-frequency = <48000000>;
	m25p80@0 {
		compatible = "n25q256a";
		spi-max-frequency = <48000000>;
		reg = <0>;
		spi-cpol;
		spi-cpha;
		spi-tx-bus-width = <1>;
		spi-rx-bus-width = <1>;
		#address-cells = <1>;
		#size-cells = <1>;

		partition@0 {
			label = "QSPI.U_BOOT";
			reg = <0x00000000 0x000080000>;
		};
		partition@1 {
			label = "QSPI.U_BOOT.backup";
			reg = <0x00080000 0x00080000>;
		};
		partition@2 {
			label = "QSPI.U-BOOT-SPL_OS";
			reg = <0x00100000 0x00010000>;
		};
		partition@3 {
			label = "QSPI.U_BOOT_ENV";
			reg = <0x00110000 0x00010000>;
		};
		partition@4 {
			label = "QSPI.U-BOOT-ENV.backup";
			reg = <0x00120000 0x00010000>;
		};
		partition@5 {
			label = "QSPI.KERNEL";
			reg = <0x00130000 0x0800000>;
		};
		partition@6 {
			label = "QSPI.KERNEL-DTB";
			reg = <0x00930000 0x0020000>;
		};
		partition@7 {
			label = "QSPI.FILESYSTEM";
			reg = <0x00950000 0x16D0000>;
		};
	};
};
*/

&mac {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&cpsw_default>;
	pinctrl-1 = <&cpsw_sleep>;
	status = "okay";
};

&davinci_mdio {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&davinci_mdio_default>;
	pinctrl-1 = <&davinci_mdio_sleep>;
	status = "okay";
};

&cpsw_emac0 {
/*	phy_id = <&davinci_mdio>, <0>;	//DIO_ver01a & DIO_ver01b*/
	phy_id = <&davinci_mdio>, <1>;	/*DIO_ver01c - change PHY to DP series*/
	phy-mode = "gmii";
};

&rtc {
	status = "okay";
	ext-clk-src;
};

&wdt {
	status = "okay";
};

&cpu {
	cpu0-supply = <&tps65218>;
	ti,opp-disable-exception = <300000>;
};

&sgx {
	status = "okay";
};

&edma {
          ti,edma-xbar-event-map = /bits/ 16 <
		16 38
                17 39
		18 40
		19 41
		53 53 
		54 54
		57 55
		58 56
		55 4
		56 7
		59 14
		60 15>;
};

&timer4 {
/*	compatible = "ec_spi_timer"; */
	pinctrl-names = "default";
	pinctrl-0 = <&timer4_pins>;
	status = "okay";
};

&spi0 {
	compatible = "expaio";
	pinctrl-names = "default";
	pinctrl-0 = <&spi0_pins>;
	status = "okay";
	ti,spi-num-cs = <2>;
	dmas = <&edma 16 &edma 17 &edma 18 &edma 19>;
	dma-names = "tx0", "rx0", "tx1", "rx1";
	ti,pindir-d0-out-d1-in = <1>;
	status = "okay";
	spi-frequency = <16000000>;

	/*FPGA machxo2*/
	spi0_1@0x01{
		compatible="lattice,machxo2";
		spi-max-frequency = <2500000>;
		spi-cpol = <1>;
		spi-cpha = <1>;
		reg = <1>;
		configflash {
			linux,mtd-name = "mainboard-fpga-bitsream";
		};
		userflash {
			linux,mtd-name = "mainboard-fpga-userflash";
		};
	};
};
&pruss1 {
	pinctrl-names = "default";
	pinctrl-0 = <&mdio_pruss11_pins_default>;
	status = "okay";
};

&gpmc {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&gpmc_pins>;
	ranges = <0 0 0x01000000 0x00000010>;	/* CS0: 16B */
	ecpu@0,0 {
		compatible = "arkus,ecpu";
		bank-width = <2>;
		reg = <0 0 4>;
		gpmc,mux-add-data = <2>;
		gpmc,device-width = <2>;
		gpmc,cs-on-ns = <0>;
		gpmc,cs-rd-off-ns = <44>;
		gpmc,cs-wr-off-ns = <44>;
		gpmc,adv-on-ns = <6>;
		gpmc,adv-rd-off-ns = <34>;
		gpmc,adv-wr-off-ns = <44>;
		gpmc,we-on-ns = <0>;
		gpmc,we-off-ns = <40>;
		gpmc,oe-on-ns = <0>;
		gpmc,oe-off-ns = <54>;
		gpmc,access-ns = <64>;
		gpmc,rd-cycle-ns = <82>;
		gpmc,wr-cycle-ns = <82>;
		gpmc,bus-turnaround-ns = <0>;
		gpmc,cycle2cycle-delay-ns = <0>;
		gpmc,clk-activation-ns = <0>;
		gpmc,wr-access-ns = <40>;
		gpmc,wr-data-mux-bus-ns = <0>;
	};
};

