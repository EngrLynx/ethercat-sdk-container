#! /bin/bash

cd /home/dev/${SRC_REL_PATH}/

# Update u-boot
patch -p0 -i changes/u-boot.patch

# Update kernel
KERNEL_MOD="\
    arch/arm/boot/dts/am437x-ethercat.dts \
    arch/arm/configs/am437x_ethercat_defconfig \
    drivers/ecpu/Kconfig \
    drivers/ecpu/Makefile \
    drivers/ecpu/ecpu.c \
    drivers/ecpu/ecpu.h \
    "

cd changes/kernel
cp --parents ${KERNEL_MOD} ../../kernel
cd -

# Update rootfs
ROOTFS_MOD="\
    configs/am437x_ethercat_defconfig \
    "

cd changes/rootfs
cp --parents ${ROOTFS_MOD} ../../rootfs
cd -
