#! /bin/bash

# ToDo: Check that ${MAKEDIR} exists.

export CROSS_COMPILE=arm-linux-gnueabihf-
export CWD=`pwd`
export MAKEDIR=/home/dev/${SRC_REL_PATH}/u-boot

if ! ${CROSS_COMPILE}gcc --version 2>/dev/null; then
    echo "Cross compiler not found. Make sure you are inside the SDK container."
    exit 1
fi

if [ ! -d "${MAKEDIR}" ]; then
    echo "Directory to be built ${MAKEDIR} not found."
    exit 1
fi

# Make and deploy UART u-boot
cd ${MAKEDIR}
make distclean
make am43xx_evm_defconfig
make
make deploy-uart
# Make and deploy QSPI u-boot
make distclean
make am43xx_evm_qspiboot_defconfig
make
make deploy-qspi
cd ${CWD}
