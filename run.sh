#! /bin/bash

if [ -f "common-config.sh" ]; then
    source common-config.sh
else
    echo "common-config.sh does not exist"
    exit 1
fi

if [ -f "run-config.sh" ]; then
    source run-config.sh
else
    echo "run-config.sh does not exist"
    exit 1
fi

docker images | grep -w ${IMAGE_NAME}
if [[ $? != 0 ]] ; then
    echo "${IMAGE_NAME} not found. Please build the image first."
    exit 1
fi

docker-compose up -d

while [ ! -f "${SRC_REL_PATH}/u-boot/Makefile" ]; do
    sleep 1
done
if ! grep --quiet "# Deploy" "${SRC_REL_PATH}/u-boot/Makefile"; then
    sed -i "/^\s@echo\s\$(UBOOTVERSION)$/r make-upd/u-boot.upd" ${SRC_REL_PATH}/u-boot/Makefile
fi

while [ ! -f "${SRC_REL_PATH}/kernel/Makefile" ]; do
    sleep 1
done
if ! grep --quiet "# Deploy" "${SRC_REL_PATH}/kernel/Makefile"; then
    sed -i "/^\s@echo\s\$(KERNELVERSION)$/r make-upd/kernel.upd" ${SRC_REL_PATH}/kernel/Makefile
fi

while [ ! -f "${SRC_REL_PATH}/rootfs/Makefile" ]; do
    sleep 1
done
if ! grep --quiet "# Deploy" "${SRC_REL_PATH}/rootfs/Makefile"; then
    sed -i "/^\s@echo\s\$(BR2_VERSION_FULL)$/r make-upd/rootfs.upd" ${SRC_REL_PATH}/rootfs/Makefile
fi

docker attach ${CONT_NAME}
